/*  EEPROM LAYOUT
                  "BOOTMODE;SSID;PASSWORD;HOSTNAME;"
    BOOTMODE could be 0 for Setup, 1 for normal use, 2 for OTA
    Setup mode is trigger by setting GPIO3 to ground or at first boot
*/


int saveConfig(int bootMode, String ssid, String password, String host ) {
  String eeprom;

  eeprom = String(bootMode) + ";" + ssid + ";" + password + ";" + host + ";";

  if (eeprom.length() > EEPROM_SIZE )
    return -EMSGSIZE;

  Serial.println("Saving " + eeprom);

  for (int i = 0; i < eeprom.length() && i < EEPROM_SIZE; i++) {
    EEPROM.write(i, eeprom.charAt(i));
  }

  EEPROM.commit();
  delay(100);

  return 0;
}

int saveBootMode(int bootMode){
  EEPROM.write(0,String(bootMode).charAt(0));
  EEPROM.commit();
  delay(100);
  return 0;
}

void readEEPROM(int &bootMode, char **ssid, char **password, char **host) {

  int i = 2;

  int boot = EEPROM.read(0);

  if (boot == '1') {
    bootMode = BOOTMODE_NORMAL;
  } else if (boot == '2') {
    bootMode = BOOTMODE_OTA;
  } else {
    //Do not need to parse EEPROM when not configured
    bootMode = BOOTMODE_SETUP;
    return;
  }

  //Read SSID
  *ssid = &eeprom[2];
  do {
    eeprom[i] = EEPROM.read(i);
    i++;
  } while (i < EEPROM_SIZE && eeprom[i - 1] != ';');
  eeprom[i - 1] = '\0';

  //Read password
  *password = &eeprom[i];
  do {
    eeprom[i] = EEPROM.read(i);
    i++;
  } while (i < EEPROM_SIZE && eeprom[i - 1] != ';');
  eeprom[i - 1] = '\0';

  //Read HostName
  *host = &eeprom[i];
  do {
    eeprom[i] = EEPROM.read(i);
    i++;
  } while (i < EEPROM_SIZE && eeprom[i - 1] != ';');
  eeprom[i - 1] = '\0';

}
