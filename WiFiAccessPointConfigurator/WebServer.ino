void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected</h1><br/>"
              "<a href=\"/setup\">Setup</a><br/>"
              "<a href=\"/otamode\">OTA mode</a><br/>"
              "<a href=\"/gpio?gpio=2&amp;value=1\">ON</a><br/>"
              "<a href=\"/gpio?gpio=2&value=0\">OFF</a><br/>"
             );
}

void handleSetup() {
  server.send(200, "text/html", "<form action=\"/save\" method=\"get\">"
              "<div><label for=\"ssid\">Wifi SSID :</label> <input type=\"text\" name=\"ssid\" /></div>"
              "<div><label for=\"password\">Wifi Password :</label><input type=\"password\" name=\"password\" /> </div>"
              "<div><label for=\"host\">Hostname :</label><input type=\"text\" name=\"host\" /> </div>"
              "<div class=\"button\"> <button type=\"submit\">Save</button></div>"
              "</form>");
}

void handleGpio() {
  if (!server.hasArg("gpio") || !server.hasArg("value")) {
    server.send(500, "text/plain", "Bad arguments\r\n");
    return;
  }

  pinMode(server.arg("gpio").toInt(), OUTPUT);
  digitalWrite(server.arg("gpio").toInt(), server.arg("value").toInt());
  server.send(200, "text/html", "<h1>GPIO" + server.arg("gpio") + " changed to " + server.arg("value") + "</h1>");
}

void handleSave() {
  String password;
  String ssid;
  String hostName;

  if (!server.hasArg("ssid") || !server.hasArg("password") || !server.hasArg("host")) {
    server.send(500, "text/plain", "Bad arguments\r\n");
    return;
  }

  if (saveConfig(BOOTMODE_NORMAL, server.arg("ssid"), server.arg("password"), server.arg("host")) < 0) {
    server.send(500, "text/plain", "Cannot Save Credentials (Too long ?Contains \";\"?)\r\n");
    return;
  }

  server.send(200, "text/html", "<h1>Configuration Saved</h1><br/>"
              "You can reboot now");
}

void handleOTA() {
  Serial.println("Boot mode Set to OTA");
  saveBootMode(BOOTMODE_OTA);
  server.send(200, "text/html", "<h1>OTA Mode set</h1><br/>"
              "You can reboot now");
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setupWebServer(int bootmode) {
  server.on("/", handleRoot);
  server.on("/setup", handleSetup);
  server.on("/save", handleSave);
  server.on("/gpio", handleGpio);
  server.on("/otamode", handleOTA);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");
}


