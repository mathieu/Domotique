[![build status](https://gitlab.mathux.org/Mathieu/Domotique/badges/master/build.svg)](https://gitlab.mathux.org/Mathieu/Domotique/commits/master)

# Introduction

This is a bunch of project that aims to provide a reusable platform for different kind of project using ESP8266.

Start by looking at ESP8266 SDK integration in Arduino environment https://github.com/esp8266/Arduino

## WiFiWebServer
Provide GPIO control by HTTP request

## WiFiAccessPointConfigurator
Provide GPIO control by HTTP request.

To be able to configure the Wifi credentials without flashing the device, at first boot, the device will create a AP that will help to configure:
* Wifi SSID to connect to
* Wifi Passwd to use for SSID
* A Mdns name to be accessible without IP address (e.g. under Linux : mydevice.local )

Settings can be reconfigured latter by web interface or by pulling down gpio 3

Device can also be put in OTA mode and will wait for OTA from the espota tool.

## WifiControlSensor
Provide previous WiFiAccessPointConfigurator features and can also get measure from several sensors:
* BMP180
* DHT11/22
* Any Analog sensor

Those measure can be shared by MQTT(s). MQTT details (server, username, passwd, port) can be configured by a web page.

To interface with BMP180, the following library should be installed into Arduino environment: https://github.com/mmaret/BMP180_Breakout_Arduino_Library/archive/master.zip

To use mqtt, a fork of the Adafruit Mqtt library should be installed (gitlab@gitlab.mathux.org:Mathieu/adaAdafruit_MQTT_Libraryfruit_mqtt_library.git
 -b retain_support).
This have been tested against a mosquitto server

Quick cmdline to install deps:

 cd ~/Arduino/libraries/
 git clone gitlab@gitlab.mathux.org:Mathieu/ESP8266_HIB.git
 git clone git@github.com:mmaret/BMP180_Breakout_Arduino_Library.git
 git clone gitlab@gitlab.mathux.org:Mathieu/adaAdafruit_MQTT_Libraryfruit_mqtt_library.git -b support_retain
 arduino --install-library "Adafruit BME680 Library"
 arduino --install-library "Adafruit Unified Sensor"
 arduino --install-library "DHT sensor library"
 arduino --install-library "BSEC Software Library" # Then you have to follow the instruction in the README.md in the BSEC install folder
