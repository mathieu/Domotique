#!/usr/bin/env python

# Gui to get parameters for espota.py
# espota.py is available https://raw.githubusercontent.com/esp8266/Arduino/master/tools/espota.py
# and should be in the same directory
from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import *
from tkinter.ttk import *
from threading import Thread
import urllib.request
import time

import subprocess

class Updater(Thread):
    def __init__(self, gui, ip, file):
        Thread.__init__(self)
        self.gui = gui
        self.ip = ip
        self.file = file

    def run(self):
        self.gui.installButton['state'] = 'disabled'
        self.gui.pb["value"] = 1
        try:
            print("Put device in OTA mode")
            urllib.request.urlopen("http://" + self.ip + "/otamode").read()

            self.gui.pb["value"] = 2
            print("Uploading new firmware")
            self.gui.pb["value"] = 3

            subprocess.call(["python", "espota.py", "-i",
                            self.ip, "-f", self.file])
        except Exception as e:
            showerror("Error", e)
        else:
            showinfo("Done", "Update installed")
        finally:
            self.gui.pb["value"] = 4
            self.gui.installButton['state'] = 'normal'


class UpdaterGui(Frame):
    file = "firmware.bin"
    sleep = 0
    installButton = None

    def __init__(self, win, **kwargs):
        Frame.__init__(self, win, **kwargs)
        button_opt = {'fill': constants.BOTH, 'padx': 5, 'pady': 5}
        Button(win, text='Select firmware',
               command=self.askopenfile).pack(**button_opt)

        self.ipEntry = Entry(win)
        self.ipEntry.pack()
        self.ipEntry.delete(0, END)
        self.ipEntry.insert(0, "192.168.0.XX")

        self.installButton = Button(win, text='Install', command=self.install)
        self.installButton.pack(pady=20)
        self.pb = Progressbar(win, orient='horizontal', mode='determinate', maximum=4)
        self.pb["value"] = 0
        self.pb.pack()

    def install(self):
        if self.file is None:
            showerror("Error", "Select a firmware first")
            return

        self.pb["value"] = 0
        ip = self.ipEntry.get()
        print("Installing", self.file, "at ip", ip)
        installTh = Updater(self, ip, self.file)
        installTh.start()

    def askopenfile(self):
        self.file = filedialog.askopenfilename(
            title='Select Firmware', filetypes=[('binaries', '*.bin')])

root = Tk()
root.title("Firmware Updater")

updater = UpdaterGui(root)
updater.mainloop()
