#ifdef CONFIG_ENABLE_BMP180
#include "BMP180.h"
SFE_BMP180 bmp180;
int bmp180Connected = 0;

int BMP180Setup(int sda, int scl) {
  //Use BMP fork at https://github.com/mmaret/BMP180_Breakout_Arduino_Library/archive/master.zip
  bmp180Connected = bmp180.begin(sda, scl);
	if (!bmp180Connected){
		SKETCH_DEBUG_PRINTLN("Cannot connect to BMP180");
    return -1;
	}
  return 0;
}

bool BMP180IsConnected() {
  return bmp180Connected != 0;
}

int BMP180GetTemperature(double &t) {
  char status;
	if(!BMP180IsConnected())
		return -1;
  status = bmp180.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:
    delay(status);
    status = bmp180.getTemperature(t);
    if (status != 0)
      return 0;
  }
  return -1;
}

int BMP180GetTempAndPressure(double &t, double &p) {
  if (BMP180GetTemperature(t) == 0) {
    char status;
    status = bmp180.startPressure(3);
    if (status != 0)
    {
      delay(status);
      status = bmp180.getPressure(p, t);
      if (status != 0){
        p = bmp180.sealevel(p, ALTITUDE);
        return 0;
      }
    }
  }
  return -1;
}
#endif
