#pragma once

//FEED have the following formats /feeds/USER/DEVICE_NAME/....
#define TEMPERATURE_FEED_FORMAT       "/feeds/%s/%s/temperature"
#define PRESSURE_FEED_FORMAT          "/feeds/%s/%s/pressure"
#define TEMPERATURE_DHT_FEED_FORMAT   "/feeds/%s/%s/dht/temperature"
#define HUMIDITY_DHT_FEED_FORMAT      "/feeds/%s/%s/dht/humidity"
#define DRY_FEED_FORMAT               "/feeds/%s/%s/dry"
#define GPIO_FEED_FORMAT              "/feeds/%s/%s/gpio/%d"
#define GPIO_SET_FEED_FORMAT          "/feeds/%s/%s/gpio/%d/set"
#define PWM_FEED_FORMAT               "/feeds/%s/%s/gpio/%d"
#define PWM_SET_FEED_FORMAT           "/feeds/%s/%s/gpio/%d/set"
#define IP_FEED_FORMAT                "/feeds/%s/%s/configuration/ip/addr"
#define BME680_TEMPERATURE_FEED_FORMAT "/feeds/%s/%s/bme680/temperature"
#define BME680_PRESSURE_FEED_FORMAT    "/feeds/%s/%s/bme680/pressure"
#define BME680_HUMIDITY_FEED_FORMAT    "/feeds/%s/%s/bme680/humidity"
#define BME680_GAZ_FEED_FORMAT         "/feeds/%s/%s/bme680/gaz"
#define BME680_ALT_FEED_FORMAT         "/feeds/%s/%s/bme680/alt"
#define BME680_IAQ_FEED_FORMAT         "/feeds/%s/%s/bme680/iaq"
#define BME680_IAQ_ACC_FEED_FORMAT     "/feeds/%s/%s/bme680/iaqAcc"
#define TELEINFO_IINST_FEED_FORMAT     "/feeds/%s/%s/teleinfo/iinst"
#define TELEINFO_PAPP_FEED_FORMAT     "/feeds/%s/%s/teleinfo/papp"
#define TELEINFO_BASE_FEED_FORMAT     "/feeds/%s/%s/teleinfo/base"
#define SCD4X_TEMPERATURE_FEED_FORMAT "/feeds/%s/%s/scd4x/temperature"
#define SCD4X_HUMIDITY_FEED_FORMAT    "/feeds/%s/%s/scd4x/humidity"
#define SCD4X_CO2_FEED_FORMAT         "/feeds/%s/%s/scd4x/co2"
#ifndef CONFIG_DISABLE_MQTT
#include "Adafruit_MQTT.h"

typedef struct {uint8_t updated; int value;} gpioInfo;

struct mqttInfo {
  float value;
  const char *topic;
  uint8_t retain;
  uint8_t qos;
};
int MqttBatchPublish(std::vector<struct mqttInfo> tab, ...);
Adafruit_MQTT_Publish *MqttCreatePublisher(uint8_t qos, uint8_t retain, const char *fmt, ...);
int MqttConnect();
int MqttIsConnected();
int MqttSetup(const char *server, const char *user, const char *passwd, int port, const char * hostname);
template<typename T> int MqttPublish(Adafruit_MQTT_Publish *publisher, T value);
int MqttPublishIp(const String &ip);
void MqttCheckSubscription();
void MqttCheckIRQ();
void MqttChangeGpioValue(int gpio, int value);
void MqttChangePWMValue(int gpio, int value);
void MqttNofityIRQ(uint8_t gpio, int value);
void MqttNofity(int gpio, int value);
#else
int MqttBatchPublish(std::vector<struct mqttInfo> tab, ...){return 0;}
int MqttConnect(){return 0;}
int MqttIsConnected(){return 0;}
int MqttSetup(const char *server, const char *user, const char *passwd, int port, const char * hostname){return 0;}
template<typename T> int MqttPublish(Adafruit_MQTT_Publish *publisher, T value){return 0;}
int MqttPublishIP(const String &ip){return 0;}
void MqttCheckSubscription(){}
void MqttCheckIRQ(){}
void MqttChangeGpioValue(int gpio, int value){}
void MqttChangePWMValue(int gpio, int value){}
void MqttNofityIRQ(uint8_t gpio, int value){}
void MqttNofity(int gpio, int value){}
#endif
