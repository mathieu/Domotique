#ifdef CONFIG_ENABLE_BMP280
#include "BMP280.h"
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bmp;
int bmpConnected = 0;

int BMP280Setup()
{
	bmpConnected = bmp.begin(BMP280_ADDRESS_ALT);
	if (!bmpConnected) {
		SKETCH_DEBUG_PRINTLN("Cannot connect to BMP280");
		return -1;
	}
	/* Default settings from datasheet. */
	bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
			Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
			Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
			Adafruit_BMP280::FILTER_X16,      /* Filtering. */
			Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
	return 0;
}

bool BMP280IsConnected()
{
	return bmpConnected != 0;
}

int BMP280GetTemperature(double &t)
{
	if (!BMP280IsConnected())
		return -1;
	t = bmp.readTemperature();
	return 0;
}

int BMP280GetTempAndPressure(double &t, double &p)
{
	if (!BMP280IsConnected())
		return -1;
	t = bmp.readTemperature();
	p = bmp.readPressure()/100;
	return 0;
}
#endif
