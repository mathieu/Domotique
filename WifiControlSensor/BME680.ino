#ifdef CONFIG_ENABLE_BME680
#include "BME680.h"
Adafruit_BME680 bme; // I2C
//Adafruit_BME680 bme(BME_CS); // hardware SPI
//Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO,  BME_SCK);

int bme680Connected = 0;

int BME680Setup() {
    bme680Connected = bme.begin();
    if (!bme680Connected){
        SKETCH_DEBUG_PRINTLN("Cannot connect to BME680");
        return -1;
    }
    // Set up oversampling and filter initialization
    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
    bme.setGasHeater(320, 150); // 320*C for 150 ms
    return 0;
}

bool BME680IsConnected() {
  return bme680Connected != 0;
}

int BME680GetMeasure(float &t, float &p, float &h, float &g, float &a){
    if(!bme.performReading()){
        SKETCH_DEBUG_PRINTLN("Cannot read BME680 measure");
        return -1;
    }
    t = bme.temperature;
    p = bme.pressure / 100.0;
    h = bme.humidity;
    g = bme.gas_resistance / 1000.0;
    a = bme.readAltitude(SEALEVELPRESSURE_HPA);
    return 0;
}
#endif
