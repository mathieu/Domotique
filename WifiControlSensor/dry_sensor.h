#pragma once

#ifdef CONFIG_ENABLE_DRY_SENSOR
int DrySetup(int powerGPIO);
int DryGetMeasure(int &dry);
#else
int DrySetup(int){return -1;}
int DryGetMeasure(int){return -1;}
#endif
