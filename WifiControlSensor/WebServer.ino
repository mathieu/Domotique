#ifndef CONFIG_DISABLE_WEB
#include <ESP8266HTTPUpdateServer.h>

ESP8266HTTPUpdateServer httpUpdater;
String gpioControlHTML = "";
String pwmControlHTML = "";


void WebBuildGpioObserved(String &html) {
  if (NB_ELEMENTS(gpioObserved) > 0) {
    html += "<fieldset>"
            "<legend>Detector</legend>";
    for (uint i = 0 ; i < NB_ELEMENTS(gpioObserved) ; i++) {
      html += "Sensor " + String(gpioObserved[i]) + ": " + digitalRead(gpioObserved[i]) + "<br/>";
    }
    html  += "</fieldset>";
  }

}
void WebHandleRoot() {
  String gpioObserved = "";
  String optimiseConfig = "";

  WebBuildGpioObserved(gpioObserved);
  if (WiFi.status() == WL_CONNECTED) {
    optimiseConfig = "<a href=\"/setupPreConfig\">Optimize Config</a><br/>";
  }

  server.send(200, "text/html",
              "<head><meta http-equiv=\"refresh\" content=\"" + String(CONFIG_SAMPLING_PERIOD_MS / 1000) + "\" ></head>"
              "<h1>You are connected to " + String(conf.host) + "</h1>"
              "<fieldset>"
              "<legend>Sensors</legend>"
#if defined CONFIG_ENABLE_BMP180 || defined CONFIG_ENABLE_BMP280
              "" + ((BMP180IsConnected() || BMP280IsConnected()) ? "<h6>BMP180/280</h6>Temperature " + String(temp, 2) + "C<br/> Pressure " + String(pressure, 2) + "hPa<br/>" : "BMP180/280 Disconnected"  ) + ""
#endif
#ifdef CONFIG_ENABLE_DHT
              "" + (DHTIsConnected() ? "<h6>DHT</h6>Temperature " + String(dhtTemp, 0) + "C<br/> Humidity " + String(dhtHumidity, 0) + "%<br/>" : "DHT Disconnected"  ) + ""
#endif
#ifdef CONFIG_ENABLE_SCD4X
              "" + (SCD4XIsConnected() ? "<h6>SCD4X</h6>Temperature " + String(SCD4xT, 0) + "C<br/> Humidity " + String(SCD4xH, 0) + "%<br/> CO2 " + String(SCD4xCo2) + "ppm<br/>" : "SCD4X Disconnected"  ) + ""
#endif
#ifdef CONFIG_ENABLE_BME680
              "" + (BME680IsConnected() ? "<h6>BME680</h6>Temperature " + String(bme680T, 2) + "C<br/> Pressure " + String(bme680P, 2) + "hPa<br/> Humidity " + String(bme680H, 2) + "%<br/> Gaz " +  String(bme680G, 2) + "kOhm<br/>" : "BME680 Disconnected"  ) + ""
#endif
#ifdef CONFIG_BME680_BSEC_ENABLE
              "" + (BME680BSECIsConnected() ? "<h6>BME680 BSEC</h6>Temperature " + String(bme680BSECT, 2) + "C<br/> Pressure " + String(bme680BSECP, 2) + "hPa<br/> Humidity " + String(bme680BSECH, 2) + "%<br/> Indoor Air Quality " +  String(bme680BSECIaq, 2) + "/500<br/> IAQ Accuraccy" + String(bme680BSECIaqAcc) + "/3<br/>": "BME680 Disconnected"  ) + ""
#endif
#ifdef CONFIG_ENABLE_DRY_SENSOR
              "Dryness " + String((dryness * 100) / 1024) + "%<br/>"
#endif
#ifdef CONFIG_ENABLE_TELEINFO
              "<h6>Power Consumption</h6> Base " + String(teleBase, 2) +"Kwh<br/> AC Power " + telePapp +"VA<br/> Instant Current" +  teleIinst +"A<br/>"
#endif
              "</fieldset>" + gpioControlHTML + gpioObserved + pwmControlHTML + "<fieldset>"
              "<legend>Settings</legend>"
              "<a href=\"/setup\">Enter Setup</a><br/>"
              "<a href=\"/upload\">Update firmware</a><br/>" + optimiseConfig +
              "MQTT Status: " + (MqttIsConnected() ? "Connected" : "Disconnected") + "<br/>"
              "Wifi Strength: " + WiFi.RSSI() + "dBm<br/>"
              "Free space: " + ESP.getFreeSketchSpace() + "<br/>"
              "Free heap: " + ESP.getFreeHeap() + "<br/>"
              "Build the " + __DATE__ + " at " + __TIME__ + "<br/>"
              "</fieldset>"
             );
}

void WebSendError(const char *error) {
  server.send(500, "text/plain", error);
}

void WebBuildSSIDList(String &datalist) {
  int n = WiFi.scanNetworks();
  datalist = "<datalist id=\"scan_ssid\">";
  // sort by RSSI
  int indices[n];
  for (int i = 0; i < n; i++) {
    indices[i] = i;
  }
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n; j++) {
      if (WiFi.RSSI(indices[j]) > WiFi.RSSI(indices[i])) {
        std::swap(indices[i], indices[j]);
      }
    }
  }
  for (int i = 0; i < n; ++i) {
    datalist += "<option value=\"" + WiFi.SSID(indices[i]) + "\">";
  }
  datalist += "</datalist>";
}

void WebHandleSetupPreConfig() {
  conf.bssid = strdup( WiFi.BSSIDstr().c_str());
  conf.channel = WiFi.channel();
  conf.ip_mode = 1;
  conf.ip = WiFi.localIP();
  conf.mask = WiFi.subnetMask();
  conf.gw = WiFi.gatewayIP();
  conf.dns = WiFi.dnsIP();
  conf.dns2 = WiFi.dnsIP(1);
  WebHandleSetup();
}

void WebHandleSetup() {
  String ssidlist;
  WebBuildSSIDList(ssidlist);
  String dhcpChecked = conf.ip_mode == 0 ? "checked" : "";
  String staticChecked = conf.ip_mode == 1 ? "checked" : "";


  server.send(200, "text/html", "<form action=\"/save\" method=\"get\">"
              "<fieldset>"
              "<legend>Wifi configuration:</legend>"
              "<div><label for=\"ssid\">Wifi SSID: </label> <br/><input list=\"scan_ssid\" type=\"text\" name=\"ssid\" value=\"" + String(conf.ssid) + "\" /></div>"
              "" + ssidlist + ""
              "<div><label for=\"password\">Wifi Password: </label><br/><input type=\"password\" name=\"password\" style=\"border-color:red\" value=\"" + String(conf.password) + "\"/> </div>"
              "<div><label for=\"host\">Hostname: </label><br/><input type=\"text\" name=\"host\" value=\"" + String(conf.host) + "\" /> </div>"
              "<div><label for=\"channel\">Channel (0 for auto): </label><br/><input type=\"text\" name=\"channel\" value=\"" + String(conf.channel) + "\" /> </div>"
              "<div><label for=\"bssid\">BSSID (Empty for auto): </label><br/><input type=\"text\" name=\"bssid\" value=\"" + String(conf.bssid) + "\" /> </div>"
              "</fieldset>"
              "<fieldset>"
              "<legend>IP Configuration</legend>"
              "<div><input type=\"radio\" name=\"ip_config\" value=\"0\" " + dhcpChecked + ">DHCP <input type=\"radio\" name=\"ip_config\" value=\"1\" " + staticChecked + ">Static</div>"
              "<div><label for=\"ip\">Ip :</label><br/><input type=\"text\" name=\"ip\" value=\"" + (conf.ip == 0 ? WiFi.localIP().toString() : IPAddress(conf.ip).toString()) + "\" /> </div>"
              "<div><label for=\"gw\">Gateway :</label><br/><input type=\"text\" name=\"gw\" value=\"" + (conf.gw == 0 ? WiFi.gatewayIP().toString() : IPAddress(conf.gw).toString()) + "\" /> </div>"
              "<div><label for=\"mask\">Netmask :</label><br/><input type=\"text\" name=\"mask\" value=\"" + (conf.mask == 0 ? WiFi.subnetMask().toString() : IPAddress(conf.mask).toString()) + "\" /> </div>"
              "<div><label for=\"mask\">DNS :</label><br/><input type=\"text\" name=\"dns\" value=\"" + (conf.dns == 0 ? (WiFi.dnsIP().isSet() ? WiFi.dnsIP().toString():"") : IPAddress(conf.dns).toString()) + "\" /> </div>"
              "<div><label for=\"mask\">DNS2 :</label><br/><input type=\"text\" name=\"dns2\" value=\"" + (conf.dns2 == 0 ? (WiFi.dnsIP(1).isSet() ? WiFi.dnsIP(1).toString():"") : IPAddress(conf.dns2).toString()) + "\" /> </div>"
              "</fieldset>"
              "<fieldset>"
              "<legend>MQTT:</legend>"
              "<div><label for=\"mqttServer\">Server :</label><br/><input type=\"text\" name=\"mqttServer\" value=\"" + String(conf.mqttServer) + "\" /> </div>"
              "<div><label for=\"mqttUser\">Username :</label><br/><input type=\"text\" name=\"mqttUser\" value=\"" + String(conf.mqttUser) + "\" /> </div>"
              "<div><label for=\"mqttPasswd\">Password :</label><br/><input type=\"password\" name=\"mqttPasswd\" style=\"border-color:red\" value=\"" + String(conf.mqttPasswd) + "\" /> </div>"
              "<div><label for=\"mqttPort\">Port :</label><br/><input type=\"text\" name=\"mqttPort\" value=\"" + String(conf.mqttPort) + "\" /> (8883 for secure Mqtts) </div>"
              "</fieldset>"
              "<fieldset>"
              "<legend>Sensor:</legend>"
              "<div><label for=\"samplingPeriod\">Sampling Period (ms): </label><br/><input type=\"text\" name=\"samplingPeriod\" value=\"" + String(conf.samplingPeriod) + "\" /> </div>"
              "</fieldset>"
              "<div class=\"button\"> <button type=\"submit\">Save</button></div>"
              "</form>");
}

void WebHandleGpio() {
  if (!server.hasArg("gpio") || !server.hasArg("value")) {
    server.send(500, "text/plain", "Bad arguments\r\n");
    return;
  }

  MqttChangeGpioValue(server.arg("gpio").toInt(), server.arg("value").toInt());
  server.send(200, "text/html", "<h1>GPIO" + server.arg("gpio") + " changed to " + server.arg("value") + "</h1>");
}

void WebHandlePWM() {
  if (!server.hasArg("gpio") || !server.hasArg("value")) {
    server.send(500, "text/plain", "Bad arguments\r\n");
    return;
  }

  MqttChangePWMValue(server.arg("gpio").toInt(), server.arg("value").toInt());
  server.send(200, "text/html", "<h1>PWM" + server.arg("gpio") + " changed to " + server.arg("value") + "</h1>");
}

boolean WebSetIp(IPAddress &addr, const char *id, const char *error) {
  if (server.arg(id) != "" && !addr.fromString(server.arg(id).c_str())) {
    WebSendError(error);
    return false;
  }
  return true;
}

void WebHandleSave() {
  IPAddress ip;
  IPAddress gw;
  IPAddress mask;
  IPAddress dns;
  IPAddress dns2;

  if (!server.hasArg("ssid") || !server.hasArg("password") || !server.hasArg("host")
      || !server.hasArg("mqttServer") || !server.hasArg("mqttUser") || !server.hasArg("mqttPasswd")
      || !server.hasArg("mqttPort") || !server.hasArg("ip_config") || !server.hasArg("ip")
      || !server.hasArg("gw") || !server.hasArg("mask") || !server.hasArg("dns")
      || !server.hasArg("dns2") || !server.hasArg("channel") || !server.hasArg("bssid") || !server.hasArg("samplingPeriod")) {
    server.send(500, "text/plain", "Bad arguments\r\n");
    return;
  }


  //Check Ip configuration
  if (server.arg("ip_config").toInt() == 1) {
    if (!WebSetIp(ip, "ip", "Incorrect IP") || !WebSetIp(gw, "gw", "Incorrect Gateway") || !WebSetIp(mask, "mask", "Incorrect NetMask") ||
        !WebSetIp(dns, "dns", "Incorrect DNS") || !WebSetIp(dns2, "dns2", "Incorrect DNS2")) {
      server.send(500, "text/plain", "Bad arguments\r\n");
      return;
    }
  }

  productConfig newConf = {BOOTMODE_NORMAL,
                           strdup(server.arg("ssid").c_str()),
                           strdup(server.arg("password").c_str()),
                           strdup(server.arg("host").c_str()),
                           strdup(server.arg("mqttServer").c_str()),
                           strdup(server.arg("mqttUser").c_str()),
                           strdup(server.arg("mqttPasswd").c_str()),
                           server.arg("mqttPort").toInt(),
                           server.arg("ip_config").toInt(),
                           static_cast<uint32_t>(ip),
                           static_cast<uint32_t>(gw),
                           static_cast<uint32_t>(mask),
                           static_cast<uint32_t>(dns),
                           static_cast<uint32_t>(dns2),
                           static_cast<uint8_t>(server.arg("channel").toInt()),
                           strdup(server.arg("bssid").c_str()),
                           static_cast<uint32_t>(server.arg("samplingPeriod").toInt())};
  if (EepromSaveConfig(newConf) < 0) {
    WebSendError("Cannot Save configuration ( Credentials too long ?Contains \";\"?)\r\n");
    return;
  }
  samplingPeriod = newConf.samplingPeriod;
  if (WiFi.softAPIP() != IPAddress((uint32_t)0)) {
    //In STA mode, we can test the AP connection
    WiFi.begin(server.arg("ssid").c_str(), server.arg("password").c_str());
    server.send(200, "text/html", "<h1>Configuration Saved</h1><br/>"
                "<a href=\"/wifiStatus\">Check Wifi Configuration</a><br/>"
                "<a href=\"/reboot\">Reboot</a><br/>");
  } else {
    server.send(200, "text/html", "<h1>Configuration Saved</h1><br/>"
                "<a href=\"/reboot\">Reboot</a><br/>");
  }
}

void WebHandleOTA() {
  SKETCH_DEBUG_PRINTLN("Activating OTA mode");
  server.send(200, "text/html", "<h1>Setting OTA mode</h1><br/>"
              "Web ui will be disabled<br/>");
  mode = BOOTMODE_OTA;
  OTASetup();
}

void WebHandleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void WebHandleReboot() {
  SKETCH_DEBUG_PRINTLN("HTTP request to reboot");
  server.send(200, "text/html", "<h1>Device Reboot</h1><br/>");
  ESP.restart();
}

String statusToString(wl_status_t status) {
  switch (status) {
    case WL_IDLE_STATUS:      return String("Idle");
    case WL_NO_SSID_AVAIL:    return String("Wifi not found");
    case WL_CONNECTED:        return String("Connected");
    case WL_CONNECT_FAILED:   return String("Connection failed (Wrong password ?)");
    case WL_CONNECTION_LOST:  return String("Connection Lost");
    case WL_DISCONNECTED:     return String("Connecting");
    default: return String(status);
  }
}

void WebHandleWifiStatus() {

  String message;
  if (WiFi.status() == WL_DISCONNECTED)
    message += "<head><meta http-equiv=\"refresh\" content=\"1\" ></head>";
  message += "<h1>Wifi Connection Status</h1><br/>";
  message += "Connection to ";
  message += WiFi.SSID();
  message += ":<br/>";
  message += statusToString(WiFi.status());
  message += "<br/>";
  if (mode == BOOTMODE_SETUP && WiFi.status() == WL_CONNECTED) {
    message += "Wifi correctly setup! You can reboot now<br/>";
    message += "<a href=\"/reboot\">Reboot</a><br/>";
  }
  if (WiFi.status() == WL_CONNECT_FAILED || WiFi.status() == WL_NO_SSID_AVAIL) {
    message += "Try to reconfigure you WiFi details<br/>";
    message += "<a href=\"/setup\">Enter Setup</a><br/>";
  }
  server.send(200, "text/html", message);
}

void WebBuildGpioControl() {
  if (NB_ELEMENTS(gpioControlled) > 0) {
    gpioControlHTML += "<fieldset>"
                       "<legend>Relay</legend>";
    for (uint i = 0 ; i < NB_ELEMENTS(gpioControlled) ; i++) {
      gpioControlHTML += "Relay " + String(gpioControlled[i]) + " " + "<a href=\"/gpio?gpio=" + String(gpioControlled[i]) + "&amp;value=1\">ON</a>/";
      gpioControlHTML += "<a href=\"/gpio?gpio=" + String(gpioControlled[i]) + "&amp;value=0\">OFF</a><br/>";
    }
    gpioControlHTML += "</fieldset>";
  }
}

void WebBuildPwmControl() {
  if (NB_ELEMENTS(pwmControlled) > 0) {
    pwmControlHTML += "<fieldset>"
                      "<legend>PWM</legend>";
    for (uint i = 0 ; i < NB_ELEMENTS(pwmControlled) ; i++) {
      pwmControlHTML += "PWM " + String(pwmControlled[i]) + "<br/>";
      pwmControlHTML += "<input type=\"range\"  min=\"0\" max=\"1023\""
                        "style=\"background:#eee\""
                        "onChange=\"setPWM(this.value," + String(pwmControlled[i]) + ")\" />";
    }
    pwmControlHTML += "<script type=\"text/javascript\">"
                      "function setPWM(newValue, gpio){"
                      "   var xmlHttp = new XMLHttpRequest();"
                      "   xmlHttp.open( \"GET\", \"/pwm?gpio=\"+ gpio + \"&value=\" + newValue, true );" // false for synchronous request
                      "   xmlHttp.send( null );}"
                      "</script>";
    pwmControlHTML += "</fieldset>";
  }
}

void WebSetupServer(int ) {
  WebBuildGpioControl();
  WebBuildPwmControl();

  server.on("/", WebHandleRoot);
  server.on("/setupPreConfig", WebHandleSetupPreConfig);
  server.on("/setup", WebHandleSetup);
  server.on("/save", WebHandleSave);
  server.on("/gpio", WebHandleGpio);
  server.on("/pwm", WebHandlePWM);
  server.on("/otamode", WebHandleOTA);
  server.on("/reboot", WebHandleReboot);
  server.on("/wifiStatus", WebHandleWifiStatus);

  server.onNotFound(WebHandleNotFound);
  httpUpdater.setup(&server, "/upload");
  server.begin();
  SKETCH_DEBUG_PRINTLN("HTTP server started");
}
#else

void WebHandleRoot() {}
void WebHandleSetupPreConfig() {}
void WebHandleSetup() {}
void WebHandleGpio() {}
void WebHandleSave() {}
void WebHandleOTA() {}
void WebHandleNotFound() {}
void WebSetupServer(int bootmode) {}
#endif
