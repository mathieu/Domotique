#pragma once

typedef struct productConfig_t {
  uint8_t bootMode;
  char *ssid;
  char *password;
  char *host;
  char *mqttServer;
  char *mqttUser;
  char *mqttPasswd;
  int mqttPort;
  int ip_mode;
  uint32_t ip;
  uint32_t gw;
  uint32_t mask;
  uint32_t dns;
  uint32_t dns2;
  uint8_t channel;
  char *bssid;
  uint32_t samplingPeriod;

} productConfig;

int EepromSaveConfig(productConfig &config);
int EepromSaveBootMode(uint8_t bootMode);
void EepromReadConfig(productConfig &config);
int EepromSaveBME680State(uint8_t *bsecState);
int EepromLoadBME680State(uint8_t *bsecState);
