#ifdef CONFIG_ENABLE_SCD4X
#include "SCD4X.h"
#include <Arduino.h>
#include <SensirionI2CScd4x.h>
#include <Wire.h>

SensirionI2CScd4x scd4x;

int SCD4XConnected = 0;

int SCD4XSetup()
{
    Wire.begin();
    scd4x.begin(Wire);

    // Stop previous measurement
    uint16_t error = scd4x.stopPeriodicMeasurement();
    if (error) {
        SKETCH_DEBUG_PRINTLN("Cannot connect to SCD4X");
        return -1;
    }

    // Start new measurement
    error = scd4x.startPeriodicMeasurement();
    if (error) {
        SKETCH_DEBUG_PRINTLN("Cannot start measurement for SCD4X");
        return -1;
    }
    SCD4XConnected = 1;

    return 0;
}
int SCD4XGetMeasure(float &temperature, float &humidity, uint16_t &co2)
{
    // Read Measurement
    bool isDataReady = false;

    uint16_t error = scd4x.getDataReadyFlag(isDataReady);
    if (error) {
        SKETCH_DEBUG_PRINTLN("Error trying to execute getDataReadyFlag() for SCD4X ");
        return -1;
    }
    if (!isDataReady) {
        return -1;
    }
    error = scd4x.readMeasurement(co2, temperature, humidity);
    if (error || co2 == 0) {
        char errorMsg[256];
        SKETCH_DEBUG_PRINT("Error with reading measurement. Error : ");
        errorToString(error, errorMsg, sizeof(errorMsg));
        SKETCH_DEBUG_PRINTF(" Co2: %d\n", co2);

        return -1;
    }
    return 0;
}
bool SCD4XIsConnected()
{
    return SCD4XConnected != 0;
}
#endif
