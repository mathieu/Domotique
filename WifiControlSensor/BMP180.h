#pragma once
#ifdef CONFIG_ENABLE_BMP180
#include <SFE_BMP180.h>
#include "debug_sketch.h"

// Get Current altitude with http://fr.mygeoposition.com/
#define ALTITUDE 130
int BMP180GetTemperature(double &t);
int BMP180GetTempAndPressure(double &t, double &p);
int BMP180Setup(int sda, int scl);
bool BMP180IsConnected();

#else //CONFIG_ENABLE_BMP80
int BMP180GetTemperature(double &){return -1;};
int BMP180GetTempAndPressure(double &, double &){return -1;};
int BMP180Setup(int , int ){SKETCH_DEBUG_PRINTLN("BMP180 is disabled at build time"); return -1;};
bool BMP180IsConnected(){return false;};
#endif
