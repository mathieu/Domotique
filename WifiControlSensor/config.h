#pragma once
#include "config_device.h"

/* DO NOT CHANGE THIS FILE                                           */
/* Contains values that SHOULD be defined to have the sketch working */
/* Modify value in config_device.h instead                           */

#ifndef CONFIG_WEB_DELAY_MS
#define CONFIG_WEB_DELAY_MS 100
#endif

#ifndef CONFIG_SAMPLING_PERIOD_MS
#define CONFIG_SAMPLING_PERIOD_MS 60000
#endif

#if defined(CONFIG_ENABLE_BME680) && defined(CONFIG_BME680_BSEC_ENABLE)
#error "BME680 and BME680 BSEC cannot be enabled together"
#endif

#ifndef CONFIG_BME680_BSEC_I2C_ADDR
#define CONFIG_BME680_BSEC_I2C_ADDR 0x76
#endif

#if defined(CONFIG_ENABLE_BMP180) && !defined(CONFIG_BMP180_SDA)
#error "When enabling BMP180, you should configure SDA pin"
#elif !defined(CONFIG_ENABLE_BMP180)
#define CONFIG_BMP180_SDA 0
#endif

#if defined(CONFIG_ENABLE_BMP180) && !defined(CONFIG_BMP180_SCL)
#error "When enabling BMP180, you should configure SLC pin"
#elif !defined(CONFIG_ENABLE_BMP180)
#define CONFIG_BMP180_SCL 0
#endif

#if defined(CONFIG_ENABLE_DHT) && !defined(CONFIG_DHT_PIN)
#error "When enabling DHT, you should configure SDA pin"
#elif !defined(CONFIG_ENABLE_DHT) && !defined(CONFIG_DHT_PIN)
#define CONFIG_DHT_PIN 0
#endif

#ifndef CONFIG_DRY_POWER_PIN
#define CONFIG_DRY_POWER_PIN -1
#endif

#ifndef CONFIG_SSID_NAME
#define CONFIG_SSID_NAME "ESPConfigurator"
#endif

#ifndef CONFIG_CONTROLLED_PWM
#define CONFIG_CONTROLLED_PWM {}
#endif

#ifndef CONFIG_OBSERVED_GPIO
#define CONFIG_OBSERVED_GPIO {}
#endif

#ifndef CONFIG_CONTROLLED_GPIO
#define CONFIG_CONTROLLED_GPIO {}
#endif

#ifndef CONFIG_EEPROM_SIZE
#define CONFIG_EEPROM_SIZE 256
#endif

#ifndef CONFIG_SETUP_GPIO
#define CONFIG_SETUP_GPIO 14
#endif

#ifndef CONFIG_DHT_TYPE
#define CONFIG_DHT_TYPE DHT11
#endif

#if CONFIG_SETUP_GPIO == 3 || CONFIG_SETUP_GPIO == 1
#define CONFIG_SERIAL_SHOULD_SWAP
#endif

#if defined(CONFIG_ENABLE_TELEINFO)
#warning "TELEINFO is using SERIAL for communication. Debug will be on Serial1 (D4)"
#define DEBUG_PRINTER_WIFICONTROLSENSOR Serial1
#if defined(CONFIG_SERIAL_SHOULD_SWAP)
#error "When enabling TELEINFO, SERIAL_SHOULD_SWAP cannot be enabled (SETUP_GPIO == 1 or 3)"
#endif
#if defined(CONFIG_ENABLE_EXTRA_GPIO)
#error "When enabling TELEINFO, ENABLE_EXTRA_CPIO cannot be enabled"
#endif
#endif
