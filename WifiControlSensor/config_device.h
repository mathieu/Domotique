#pragma once
#include "pins_arduino.h"
// Enable Serial Console (Disable to save space and power)
#define CONFIG_SKETCH_DEBUG

// Switch Serial console on gpio 13 and 15 (So you can use GPIO 1 and 3 for other things)
//#define CONFIG_ENABLE_EXTRA_GPIO

// Disable SSL (so mqtts) to save some place (~52ko)
//#define CONFIG_DISABLE_SSL
//#define CONFIG_DISABLE_WEB
//#define CONFIF_DISABLE_OTA
//#define CONFIG_DISABLE_MQTT

// Enable the temperature, pressure, humidity and gaz Sensor BME680 on standard i2c esp8266 pins
// It use default i2c pin GPIO4(D2): SDA, GPIO5(D1):SCL
// Should be powered by 3.3v and sampled every 3sec or 300s (Or you should adapt bsec_config_iaq in BME680_BSEC.ino )
//#define CONFIG_BME680_BSEC_ENABLE
#define CONFIG_BME680_BSEC_I2C_ADDR 0x77

// Enable the temperature, pressure, humidity and gaz Sensor BME680 on standard i2c esp8266 pins
// It use default i2c pin GPIO4(D2): SDA, GPIO5(D1):SCL
//#define CONFIG_ENABLE_BME680
// Enable the temperatue and pressure Sensor BMP180 
// (CONFIG_BMP180_SDA and CONFIG_BMP180_SDA should be defined as well)
//#define CONFIG_ENABLE_BMP180
//#define CONFIG_BMP180_SDA SDA //D2
//#define CONFIG_BMP180_SCL SCL //D1
//
//#define CONFIG_ENABLE_DHT
//#define CONFIG_DHT_PIN 2
//#define CONFIG_DHT_TYPE DHT22
//#define CONFIG_ENABLE_BMP280

//#define CONFIG_ENABLE_DRY_SENSOR
//If the dry sensor is powered by a GPIO, this GPIO could be defined here
//#define CONFIG_DRY_POWER_PIN 13

//Advised CONFIG_SAMPLING_PERIOD_MS is 5s for this sensor
//#define CONFIG_ENABLE_SCD4X

// Enable light sleep to save some power (http://bbs.espressif.com/viewtopic.php?f=6&t=133&p=485&hilit=sleep+modem#p485)
#define CONFIG_ENABLE_POWER_SAVE

// Disable mDNS can also save power
#define CONFIG_ENABLE_MDNS

// Long press on this button will put device in setup mode at runtime
#define CONFIG_SETUP_BUTTON 0

// Teleinfo https://github.com/hallard/LibTeleinfo/
#define CONFIG_ENABLE_TELEINFO

/* DEFAULT VALUE ALSO DEFINED IN CONFIG.H */
//If this GPIO is LOW at boot, device will enter setup mode
#define CONFIG_SETUP_GPIO 14 //D5

// Time to sleep between 2 webserver request (increase it reduce battery usage but increase latency)
#define CONFIG_WEB_DELAY_MS 800

// Get sensors value every X ms
#define CONFIG_SAMPLING_PERIOD_MS 30000

// Name of the SSID when in AP mode for configuration
#define CONFIG_SSID_NAME "ESPConfiguratorBureau"

// GPIO that can be set or get by mqtt and set via http
// Should have less value than MAXSUBSCRIPTIONS
//#define CONFIG_CONTROLLED_GPIO {12,13}

// GPIO that can be get by mqtt and http
// Pin 6 to 11 and 16 can not be used for mqtt
//#define CONFIG_OBSERVED_GPIO {5}

// GPIO used in PWM
//#define CONFIG_CONTROLLED_PWM {}

// EEPROM SIZE
// Max is 4096, but this amount will be allocated in RAM for reading its content
//#CONFIG_EEPROM_SIZE 256
