#pragma once
#include "debug_sketch.h"

#ifdef CONFIG_ENABLE_TELEINFO
#include <LibTeleinfo.h>
int TeleinfoSetup();
int TeleinfoRetrieve(float &iinst, float &papp, float &base);
int TeleinfoRetrieve(float &iinst, float &papp, float &base,
                    std::vector<struct mqttInfo> &batchInfo);
#else
int TeleinfoSetup() {
  SKETCH_DEBUG_PRINTLN("Teleinfo is disabled at build time");
  return -1;
};

int TeleinfoRetrieve(float &, float &, float &){
  return 0;
}
int TeleinfoRetrieve(float &, float &, float &, std::vector<struct mqttInfo> &)
{
  return 0;
}
#endif
