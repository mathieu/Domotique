#ifdef CONFIG_ENABLE_TELEINFO
#include "Teleinfo.h"
#include <LibTeleinfo.h>
#define TELESerial Serial
TInfo tinfo;

int TeleinfoSetup()
{
  TELESerial.begin(1200, SERIAL_7E1);

  tinfo.init();

  return 0;
}


int TeleinfoRetrieve(float &iinst, float &papp, float &base){
  int c;

  while ((c = TELESerial.read()) >= 0) {
    tinfo.process(c);
  }
  ValueList *me = tinfo.getList();
  if (me)
    me = me->next;
  while (me) {
    if (strcmp(me->name, "IINST") == 0) {
      iinst = atof(me->value);
    }
    if (strcmp(me->name, "PAPP") == 0) {
      papp = atof(me->value);
    }
    if (strcmp(me->name, "BASE") == 0) {
      float tmp = atof(me->value);
      if(tmp != 0){
        base = tmp;
      }
    }
    me = me->next;
  }

  return 0;
}

int TeleinfoRetrieve(float &iinst, float &papp, float &base,
                    std::vector<struct mqttInfo> &batchInfo)
{
  int c;

  while ((c = TELESerial.read()) >= 0) {
    tinfo.process(c);
  }
  ValueList *me = tinfo.getList();
  if (me)
    me = me->next;
  while (me) {
    if (strcmp(me->name, "IINST") == 0) {
      iinst = atof(me->value);
      batchInfo.push_back({iinst, TELEINFO_IINST_FEED_FORMAT, 0, 0});
    }
    if (strcmp(me->name, "PAPP") == 0) {
      papp = atof(me->value);
      batchInfo.push_back({papp, TELEINFO_PAPP_FEED_FORMAT, 0, 0});
    }
    if (strcmp(me->name, "BASE") == 0) {
      float tmp = atof(me->value);
      if(tmp != 0){
        base = tmp;
        batchInfo.push_back({base, TELEINFO_BASE_FEED_FORMAT, 0, 0});
      }
    }
    me = me->next;
  }

  return 0;
}
#endif
