#pragma once
#ifdef CONFIG_ENABLE_BMP280
#include "debug_sketch.h"

int BMP280GetTemperature(double &t);
int BMP280GetTempAndPressure(double &t, double &p);
int BMP280Setup();
bool BMP280IsConnected();

#else //CONFIG_ENABLE_BMP80
int BMP280GetTemperature(double &){return -1;};
int BMP280GetTempAndPressure(double &, double &){return -1;};
int BMP280Setup(){SKETCH_DEBUG_PRINTLN("BMP280 is disabled at build time"); return -1;};
bool BMP280IsConnected(){return false;};
#endif
