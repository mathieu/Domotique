#ifdef CONFIG_ENABLE_DHT
#include "sensor_DHT.h"
int DHTSetup(int pin){
	dht = new DHT(pin, DHTTYPE);
	dht->begin();
	return 0;
}

int DHTGetTempAndHumidity(float &t, float &h){
	if(!DHTIsConnected())
		goto err;
	t = dht->readTemperature();
	h = dht->readHumidity();
	if(isnan(t) || isnan(h))
		goto err;
	return 0;
err:
	t=0;
	h=0;
	return -1;
}

bool DHTIsConnected(){
	//No way to know if connected
	//Check at least if initialized
	return dht != NULL;
}
#endif
