#pragma once

#define NB_ELEMENTS(x) (sizeof(x)/ sizeof(x[0]))

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define findIndex(x,y) findIdx(x, y, NB_ELEMENTS((y)))

int findIdx(int el, const int array[], uint size){
  for (uint i = 0; i < size; i++) {
    if (el == array[i])
      return i;
  }
  return -1;
}
