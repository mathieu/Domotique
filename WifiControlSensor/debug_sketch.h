#pragma once
//#define CONFIG_SKETCH_DEBUG

// Set where debug messages will be printed.
#ifndef DEBUG_PRINTER_WIFICONTROLSENSOR
#define DEBUG_PRINTER_WIFICONTROLSENSOR Serial
#endif

#ifdef CONFIG_SKETCH_DEBUG
  #define SKETCH_DEBUG_INIT(speed){ DEBUG_PRINTER_WIFICONTROLSENSOR.begin(speed); }
  #define SKETCH_DEBUG_PRINT(...) { DEBUG_PRINTER_WIFICONTROLSENSOR.print(__VA_ARGS__); }
  #define SKETCH_DEBUG_PRINTF(...) { DEBUG_PRINTER_WIFICONTROLSENSOR.printf(__VA_ARGS__); }
  #define SKETCH_DEBUG_PRINTLN(...) { DEBUG_PRINTER_WIFICONTROLSENSOR.println(__VA_ARGS__); }
#else
  #define SKETCH_DEBUG_INIT(speed)
  #define SKETCH_DEBUG_PRINT(...) {}
  #define SKETCH_DEBUG_PRINTF(...) {}
  #define SKETCH_DEBUG_PRINTLN(...) {}
#endif

