#ifdef CONFIG_BME680_BSEC_ENABLE
#include "BME680_BSEC.h"

#include "bsec.h"
/* Configure the BSEC library with information about the sensor
    18v/33v = Voltage at Vdd. 1.8V or 3.3V
    3s/300s = BSEC operating mode, BSEC_SAMPLE_RATE_LP or BSEC_SAMPLE_RATE_ULP
    4d/28d = Operating age of the sensor in days
    generic_18v_3s_4d
    generic_18v_3s_28d
    generic_18v_300s_4d
    generic_18v_300s_28d
    generic_33v_3s_4d
    generic_33v_3s_28d
    generic_33v_300s_4d
    generic_33v_300s_28d
*/
const uint8_t bsec_config_iaq[] = {
#if CONFIG_SAMPLING_PERIOD_MS == 3000
#include "config/generic_33v_3s_4d/bsec_iaq.txt"
#elif CONFIG_SAMPLING_PERIOD_MS == 300000
#include "config/generic_33v_300s_4d/bsec_iaq.txt"
#else
#error "Unsupport CONFIG_SAMPLING_PERIOD_MS (3000 and 300000 are supported)"
#endif
};

#define STATE_SAVE_PERIOD                                                      \
  UINT32_C(360 * 60 * 1000) // 360 minutes - 4 times a day
uint16_t stateUpdateCounter = 0;
Bsec iaqSensor;
uint8_t bsecState[BSEC_MAX_STATE_BLOB_SIZE] = {0};

// Helper function definitions
int checkIaqSensorStatus(void) {
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      SKETCH_DEBUG_PRINTLN("BSEC error code : " + String(iaqSensor.status));

      return -2;
    } else {
      SKETCH_DEBUG_PRINTLN("BSEC warning code : " + String(iaqSensor.status));

      return -1;
    }
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      SKETCH_DEBUG_PRINTLN("BME680 error code : " + String(iaqSensor.bme680Status));

      return -2;
    } else {
      SKETCH_DEBUG_PRINTLN("BME680 warning code : " + String(iaqSensor.bme680Status));

      return -1;
    }
  }
  iaqSensor.status = BSEC_OK;

  return 0;
}

void updateState(void)
{
  bool update = false;
  /* Set a trigger to save the state. Here, the state is saved every STATE_SAVE_PERIOD with the
   * first state being saved once the algorithm achieves full calibration, i.e. iaqAccuracy = 3
   */
  if (stateUpdateCounter == 0) {
    if (iaqSensor.iaqAccuracy >= 3) {
      update = true;
      stateUpdateCounter++;
    }
  } else {
    /* Update every STATE_SAVE_PERIOD milliseconds */
    if ((stateUpdateCounter * STATE_SAVE_PERIOD) < millis()) {
      update = true;
      stateUpdateCounter++;
    }
  }

  if (update) {
    iaqSensor.getState(bsecState);
    checkIaqSensorStatus();

    SKETCH_DEBUG_PRINTLN("Writing state to EEPROM");

    EepromSaveBME680State(bsecState);
  }
}

int BME680BSECGetMeasure(float &t, float &p, float &h, float &iaq, float &iaqAcc)
{
  if (iaqSensor.run()) { // If new data is available
    t      = iaqSensor.temperature;
    p      = iaqSensor.pressure/100;
    h      = iaqSensor.humidity;
    iaq    = iaqSensor.iaq;
    iaqAcc = iaqSensor.iaqAccuracy;
    updateState();
  } else {
    return -1;
  }
  return 0;
}

int BME680BSECSetup()
{
  Wire.begin();
  iaqSensor.begin(CONFIG_BME680_BSEC_I2C_ADDR, Wire);

  SKETCH_DEBUG_PRINTLN("\nBSEC library version " + String(iaqSensor.version.major) + "." +
           String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) +
           "." + String(iaqSensor.version.minor_bugfix));

  if (checkIaqSensorStatus())
    return -1;

  iaqSensor.setConfig(bsec_config_iaq);

  if (checkIaqSensorStatus())
    return -1;

  if (!EepromLoadBME680State(bsecState))
    iaqSensor.setState(bsecState);

  if (checkIaqSensorStatus())
    return -1;

  bsec_virtual_sensor_t sensorList[7] = {
      BSEC_OUTPUT_RAW_TEMPERATURE,
      BSEC_OUTPUT_RAW_PRESSURE,
      BSEC_OUTPUT_RAW_HUMIDITY,
      BSEC_OUTPUT_RAW_GAS,
      BSEC_OUTPUT_IAQ,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  iaqSensor.updateSubscription(sensorList, 7, BSEC_SAMPLE_RATE_LP);
  if (checkIaqSensorStatus())
    return -1;
  return 0;
}

bool BME680BSECIsConnected()
{
  if (checkIaqSensorStatus())
    return false;
  return true;
}
#endif
