#pragma once
#ifdef CONFIG_ENABLE_BME680
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include "debug_sketch.h"

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

//Use Default i2c pin GPIO4(D2): SDA, GPIO5(D1):SCL

#define SEALEVELPRESSURE_HPA (1013.25)

int BME680GetMeasure(float &t, float &p, float &h, float &g, float &a);
int BME680Setup();
bool BME680IsConnected();

#else //CONFIG_ENABLE_BME680
int BME680GetMeasure(float &, float &, float&, float &, float &){return -1;};
int BME680Setup(){SKETCH_DEBUG_PRINTLN("BME680 is disabled at build time"); return -1;};
bool BME680IsConnected(){return false;};
#endif
