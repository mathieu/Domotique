#ifdef CONFIG_ENABLE_DRY_SENSOR
#include "dry_sensor.h"
int dryGPIO;

int DrySetup(int powerGPIO){
    dryGPIO = powerGPIO;
    if(dryGPIO >= 0){
        pinMode(dryGPIO, OUTPUT);
    }
    return 0;
}

int DryGetMeasure(int &dry){
    if(dryGPIO >= 0){
        digitalWrite(dryGPIO,1);
        delay(50);
    }
    dry = analogRead(A0);
    if(dryGPIO >= 0){
        digitalWrite(dryGPIO,0);
    }
    return 0;
}
#endif
