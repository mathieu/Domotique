#pragma once
#ifdef CONFIG_BME680_BSEC_ENABLE
int BME680BSECGetMeasure(float &t, float &p, float &h, float &iaq, float &iaqAcc);
int BME680BSECSetup();
bool BME680BSECIsConnected();
#else // CONFIG_ENABLE_BME680_BSEC
#define BSEC_MAX_STATE_BLOB_SIZE 0
int BME680BSECGetMeasure(float &, float &, float &, float &, float &) {
  return -1;
};
int BME680BSECSetup() {
  SKETCH_DEBUG_PRINTLN("BME680 BSEC is disabled at build time");
  return -1;
};
bool BME680BSECIsConnected() { return false; };
#endif
