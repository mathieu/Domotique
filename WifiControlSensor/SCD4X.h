#pragma once
#ifdef CONFIG_ENABLE_SCD4X
#include "debug_sketch.h"

int SCD4XGetMeasure(float &t, float &h, uint16_t &co2);
int SCD4XSetup();
bool SCD4XIsConnected();

#else
int SCD4XGetMeasure(float &, float &, uint16_t &)
{
        return -1;
};
int SCD4XSetup()
{
        SKETCH_DEBUG_PRINTLN("SCD4X is disabled at build time");
        return -1;
};
bool SCD4XIsConnected()
{
        return false;
};
#endif
