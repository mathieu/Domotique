#pragma once
#ifdef CONFIG_ENABLE_DHT
#include <DHT.h>
#define DHTTYPE CONFIG_DHT_TYPE
DHT *dht = NULL;
int DHTSetup(int pin);
int DHTGetTempAndHumidity(float &t, float &h);
bool DHTIsConnected();

#else //CONFIG_ENABLE_DHT
int DHTSetup(int){SKETCH_DEBUG_PRINTLN("DHT is disabled at build time"); return -1;};
int DHTGetTempAndHumidity(float &, float &){return -1;};
bool DHTIsConnected(){return false;};
#endif
