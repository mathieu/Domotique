/*
 *  This sketch demonstrates how to set up a simple HTTP-like server.
 *  The server will set a GPIO pin depending on the request
 *    http://server_ip/gpioX/0 will set the GPIOX low,
 *    http://server_ip/gpioX/1 will set the GPIOX high,
 *    http://server_ip/gpioX will return GPIOX value,
 *    http://server_ip/mode/gpioX/Y will set the GPIOX mode to Y (with Y in 0=INPUT, 1=OUTPUT, 2=INPUT_PULLUP),
 *  server_ip is the IP address of the ESP8266 module, will be
 *  printed to Serial when the module is connected.
 */

#include <ESP8266WiFi.h>
#define URL_GPIO "/gpio"
#define URL_MODE "/mode"

const char* ssid = "freebox_sarah";
const char* password = "password";

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

enum requestType {INVALID, GET_VALUE, SET_VALUE, SET_MODE};

int mode[] = {INPUT, OUTPUT, INPUT_PULLUP};

struct request {
  int gpio;
  int value;
  requestType type;
};

int parseRequest(String &raw, struct request &req ) {
  int index, end;
  String url;

  index = raw.indexOf(URL_MODE);

  if ( index != -1) {
    String subUrl = raw.substring(index + strlen(URL_MODE));
    if (parseRequest(subUrl, req) >= 0) {
      if ( req.value >= 0 && req.value <= 2) {
        req.type = SET_MODE;
        req.value = mode[req.value];
        return 0;
      } else
        goto invalid;
    } else
      goto invalid;
  }

  index = raw.indexOf(URL_GPIO);

  if (index == -1)
    goto invalid;

  end = raw.indexOf(" ", index);

  if (end == -1)
    goto invalid;

  url = raw.substring(index, end);

  end = url.indexOf("/", 1);

  //URL is /gpioN something
  if (end == -1) {
    req.type = GET_VALUE;
    req.gpio = url.substring(strlen(URL_GPIO)).toInt();
  } else {
    req.type = SET_VALUE;
    req.gpio = url.substring(strlen(URL_GPIO), end).toInt();
    req.value = url.substring(end + 1).toInt();
  }
  return 0;

invalid:
  req.type = INVALID;
  return -1;

}

void setup() {
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  Serial.begin(115200);
  delay(10);

  // prepare GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, 0);

  if (WiFi.macAddress(mac) != 0) {
    for (int i = 0; i < WL_MAC_ADDR_LENGTH; i++) {
      Serial.print(mac[i], HEX);
      Serial.print((i < WL_MAC_ADDR_LENGTH - 1) ? ":" : "\n");
    }
  }

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  struct request request;

  if (parseRequest(req, request) == -1 || request.type == INVALID ) {
    Serial.println("invalid request");
    client.stop();
    return;
  }

  client.flush();

  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  if (request.type == SET_VALUE) {
    // Set GPIO according to the request
    Serial.print("Setting gpio ");
    Serial.print(request.gpio);
    Serial.print(" to ");
    Serial.println(request.value);

    //make sure this GPIO is set as output
    pinMode(request.gpio, OUTPUT);
    digitalWrite(request.gpio, request.value);

    // Prepare the response
    s += "GPIO";
    s += request.gpio;
    s += " is now ";
    s += (request.value) ? "high" : "low";
  } else if (request.type == GET_VALUE) {
    Serial.print("Getting value of GPIO");
    Serial.println(request.gpio);

    s += "GPIO";
    s += request.gpio;
    s += " is ";
    s += digitalRead(request.gpio);
  } else if (request.type == SET_MODE) {
    Serial.print("Setting mode of GPIO");
    Serial.print(request.gpio);
    Serial.print(" to mode ");
    Serial.print(request.value);

    pinMode(request.gpio, request.value);

    s += "GPIO";
    s += request.gpio;
    s += " mode is now ";
    s += (request.value);
  }

  s += "</html>\n";
  client.print(s);

  // Send the response to the client

  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected
  // when the function returns and 'client' object is detroyed
}

